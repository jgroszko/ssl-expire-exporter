use chrono::{DateTime, TimeZone, Utc};
use env_logger::{Builder, Env};
use log::{error, info};
use openssl::{
    asn1::*,
    error::ErrorStack,
    ssl::{Ssl, SslContext, SslMethod, SslVerifyMode},
};
use prometheus_exporter::prometheus::{register_gauge_vec, GaugeVec};
use std::{
    env,
    net::{SocketAddr, TcpStream, ToSocketAddrs},
    time::Duration,
};

const ENV_LISTEN_ADDR: &str = "LISTEN_ADDR";
const ENV_POLLING_INTERVAL: &str = "POLLING_INTERVAL";
const ENV_DOMAINS: &str = "DOMAINS";

const METRIC_PREFIX: &str = "ssl_expire_";

const LABEL_DOMAIN: &str = "domain";

const TIMEOUT_SECS: Duration = Duration::from_secs(30);

fn asn1_time_to_datetime(time: &Asn1TimeRef) -> Result<DateTime<Utc>, ErrorStack> {
    let unix_time = Asn1Time::from_unix(0)?.diff(time)?;
    Ok(Utc
        .timestamp_opt(unix_time.days as i64 * 86400 + unix_time.secs as i64, 0)
        .unwrap())
}

fn retrieve_metrics(metrics: &mut Vec<GaugeVec>) {
    if metrics.len() == 0 {
        metrics.push(
            register_gauge_vec!(
                format!("{}days", METRIC_PREFIX),
                "days to expiration",
                &[LABEL_DOMAIN]
            )
            .unwrap(),
        );
    }

    let domains = match env::var(ENV_DOMAINS) {
        Ok(v) => v,
        Err(_) => "tinythunk.com".to_string(),
    };

    for domain in domains.split("|") {
        let addr = format!("{}:443", domain);
        let context = {
            let mut context =
                SslContext::builder(SslMethod::tls()).expect("Could not create ssl context");
            context.set_verify(SslVerifyMode::empty());
            context.build()
        };
        let mut connector = Ssl::new(&context).expect("Could not create SSL connector");
        connector.set_hostname(domain).expect("Invalid hostname");

        match addr
            .to_socket_addrs()
            .expect("Could not parse socket address")
            .next()
        {
            Some(first_address) => {
                let stream = TcpStream::connect_timeout(&first_address, TIMEOUT_SECS)
                    .expect("Could not create stream");
                stream
                    .set_write_timeout(Some(TIMEOUT_SECS))
                    .expect("Could not set write timeout");
                stream
                    .set_read_timeout(Some(TIMEOUT_SECS))
                    .expect("Could not set read timeout");

                let stream = connector
                    .connect(stream)
                    .expect("Could not create SSL stream");
                let cert = stream
                    .ssl()
                    .peer_certificate()
                    .expect("Certificate not found");

                info!("Resolved {} to {}", addr, first_address);

                let not_after = asn1_time_to_datetime(cert.not_after())
                    .expect("Could not parse expiration date");

                let duration = not_after - Utc::now();

                metrics[0]
                    .with_label_values(&[&domain])
                    .set(duration.num_days() as f64);
            }
            None => {
                error!("Could not resolve address");
            }
        }
    }
}

fn main() {
    // Setup logger with default level info so we can see the messages from
    // prometheus_exporter.
    Builder::from_env(Env::default().default_filter_or("info")).init();

    // Parse address used to bind exporter to.
    let addr_raw = match env::var(ENV_LISTEN_ADDR) {
        Ok(v) => v,
        Err(_) => "0.0.0.0:9196".to_string(),
    };
    let addr: SocketAddr = addr_raw.parse().expect("can not parse listen addr");

    // Start exporter and update metrics every five minutes or POLLING_INTERVAL.
    let exporter = prometheus_exporter::start(addr).expect("can not start exporter");
    let duration = std::time::Duration::from_millis(match env::var(ENV_POLLING_INTERVAL) {
        Ok(v) => v.parse::<u64>().unwrap(),
        Err(_) => 1000 * 60 * 60 * 12, // default 12 hours
    });

    let mut metrics: Vec<GaugeVec> = Vec::new();

    retrieve_metrics(&mut metrics);

    ctrlc::set_handler(|| std::process::exit(0)).expect("Unable to set signal handler");

    loop {
        // Will block until duration is elapsed.
        let _guard = exporter.wait_duration(duration);

        info!("Updating metrics");

        retrieve_metrics(&mut metrics);
    }
}
